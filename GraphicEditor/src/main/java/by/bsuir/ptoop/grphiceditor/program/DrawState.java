package by.bsuir.ptoop.grphiceditor.program;

import by.bsuir.ptoop.grphiceditor.figure.Dot;
import by.bsuir.ptoop.grphiceditor.figure.Shape;
import by.bsuir.ptoop.grphiceditor.figure.ShapeHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.DrawerState;
import by.bsuir.ptoop.grphiceditor.program.plugins.GraphicsConverterHolder;

import javax.swing.*;
import java.awt.event.MouseEvent;

public class DrawState implements DrawerState {

    private ShapeHolder shapeHolder = ShapeHolder.getInstance();
    private CurrentSettings currentSettings = CurrentSettings.getInstance();
    private GraphicsConverterHolder graphicsConverterHolder = GraphicsConverterHolder.getInstance();

    private Shape currentShape;

    @Override
    public String getType() {
        return "Рисовать";
    }

    @Override
    public void changeState() {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            Dot currentDot = new Dot(e.getX(), e.getY());
            currentShape = new Shape(currentDot, currentDot, currentSettings.getCurrentDrawer().getType());
            graphicsConverterHolder.stream().forEach(currentShape::addSettings);
            shapeHolder.create(currentShape);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        currentShape = null;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        Dot currentDot = new Dot(e.getX(), e.getY());
        currentShape.setSecond(currentDot);
        shapeHolder.update(currentShape);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
