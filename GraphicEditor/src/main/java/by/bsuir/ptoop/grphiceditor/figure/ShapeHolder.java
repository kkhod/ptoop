package by.bsuir.ptoop.grphiceditor.figure;

import by.bsuir.ptoop.grphiceditor.program.plugins.Listener;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

public final class ShapeHolder {
    private List<Shape> shapes = new ArrayList<>();
    private List<Listener<Shape>> createListeners = new ArrayList<>();
    private List<Listener<Integer>> removeListeners = new ArrayList<>();
    private List<Listener<Shape>> updateListeners = new ArrayList<>();

    private ShapeHolder() {
    }

    public static ShapeHolder getInstance() {
        return ShapeHolderSingleton.INSTANCE;
    }

    public void create(Shape shape) {
        shapes.add(shape);
        createListeners.forEach(shapeListener -> shapeListener.action(shape));
    }

    public void remove(int index) {
        shapes.remove(index);
        removeListeners.forEach(shapeListener -> shapeListener.action(index));
    }

    public void remove(Shape shape) {
        int index = shapes.indexOf(shape);
        remove(index);
    }

    public void update(Shape shape) {
        updateListeners.forEach(shapeListener -> shapeListener.action(shape));
    }

    public void removeAll() {
        ListIterator<Shape> iter = shapes.listIterator();
        while (iter.hasNext()) {
            iter.next();
            iter.remove();
            removeListeners.forEach(shapeListener -> shapeListener.action(iter.previousIndex() + 1));
        }
    }

    public void createAll(List<Shape> newShapes) {
        newShapes.forEach(this::create);
    }

    public void addCreateListener(Listener<Shape> listener) {
        createListeners.add(listener);
    }

    public void addRemoveListener(Listener<Integer> listener) {
        removeListeners.add(listener);
    }

    public void addUpdateListener(Listener<Shape> listener) {
        updateListeners.add(listener);
    }

    public Stream<Shape> stream() {
        return shapes.stream();
    }

    public Stream<Shape> reverseStream() {
        return stream().collect(
                Collector.of((Supplier<ArrayDeque<Shape>>) ArrayDeque::new,
                        ArrayDeque::addFirst,
                        (a, b) -> a)
        ).stream();
    }

    private static class ShapeHolderSingleton {
        private static final ShapeHolder INSTANCE = new ShapeHolder();
    }
}
