package by.bsuir.ptoop.grphiceditor.program;

import by.bsuir.ptoop.grphiceditor.program.plugins.GraphicsConverter;

import java.awt.*;

public class HighlightConverter implements GraphicsConverter {
    @Override
    public String getName() {
        return "Highlight";
    }

    @Override
    public void convert(Graphics g, String settings) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.GRAY);
        Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
        g2.setStroke(dashed);
    }

    @Override
    public String getSettings() {
        return "Highlight";
    }

    @Override
    public boolean isRequired() {
        return false;
    }
}
