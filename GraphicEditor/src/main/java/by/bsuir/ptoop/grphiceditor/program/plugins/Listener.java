package by.bsuir.ptoop.grphiceditor.program.plugins;

@FunctionalInterface
public interface Listener<T> {
    void action(T o);
}
