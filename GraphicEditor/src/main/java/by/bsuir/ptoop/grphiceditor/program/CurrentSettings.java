package by.bsuir.ptoop.grphiceditor.program;

import by.bsuir.ptoop.grphiceditor.draw.Drawer;

import javax.swing.*;

public final class CurrentSettings {
    private Drawer currentDrawer;
    private WindowHolder windowHolder;

    private CurrentSettings() {
    }

    public static CurrentSettings getInstance() {
        return CurrentSettingsSingleton.INSTANCE;
    }

    Drawer getCurrentDrawer() {
        return currentDrawer;
    }

    void setCurrentDrawer(Drawer currentDrawer) {
        this.currentDrawer = currentDrawer;
    }

    void setInitializer(WindowHolder initializer) {
        this.windowHolder = initializer;
    }

    public void updateDrawers() {
        windowHolder.updateDrawers();
    }

    public void addAdditionalServices(JPanel additionalSettingsPanel) {
        windowHolder.addAdditionalServices(additionalSettingsPanel);
    }

    private static class CurrentSettingsSingleton {
        private static final CurrentSettings INSTANCE = new CurrentSettings();
    }
}
