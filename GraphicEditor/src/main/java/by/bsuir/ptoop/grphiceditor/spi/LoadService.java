package by.bsuir.ptoop.grphiceditor.spi;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class LoadService<T> {
    private Class<T> clazz;

    public LoadService(Class<T> clazz) {
        this.clazz = clazz;
    }

    public List<T> getServices() {
        List<T> results = new ArrayList<>();
        ServiceLoader<T> serviceLoader = ServiceLoader.load(clazz);
        serviceLoader.iterator().forEachRemaining(results::add);
        return results;
    }

    public List<T> getServices(URL url) {
        List<T> results = new ArrayList<>();
        URLClassLoader ucl = new URLClassLoader(new URL[]{url}, getClass().getClassLoader());
        ServiceLoader<T> serviceLoader = ServiceLoader.load(clazz, ucl);
        serviceLoader.iterator().forEachRemaining(results::add);
        return results;
    }
}
