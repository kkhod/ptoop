package by.bsuir.ptoop.grphiceditor.program.plugins;

import javax.swing.*;

public interface AdditionalSettingsCreator {
    JPanel create();

    GraphicsConverter getGraphicsConverter();
}
