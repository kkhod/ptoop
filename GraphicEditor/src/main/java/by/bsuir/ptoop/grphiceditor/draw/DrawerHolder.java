package by.bsuir.ptoop.grphiceditor.draw;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public final class DrawerHolder {
    private Map<String, Drawer> drawers = new HashMap<>();

    private DrawerHolder() {
    }

    public static DrawerHolder getInstance() {
        return DrawerHolderSingleton.INSTANCE;
    }

    public void register(Drawer drawer) {
        drawers.put(drawer.getType(), drawer); // TODO already exist
    }

    public Stream<Drawer> stream(){
        return drawers.values().stream();
    }

    public Drawer get(String type) {
        return drawers.get(type);
    }

    private static class DrawerHolderSingleton {
        private static final DrawerHolder INSTANCE = new DrawerHolder();
    }
}
