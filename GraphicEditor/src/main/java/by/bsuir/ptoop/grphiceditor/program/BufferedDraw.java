package by.bsuir.ptoop.grphiceditor.program;

import by.bsuir.ptoop.grphiceditor.draw.DrawerHolder;
import by.bsuir.ptoop.grphiceditor.figure.Shape;
import by.bsuir.ptoop.grphiceditor.figure.ShapeHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.DrawerState;
import by.bsuir.ptoop.grphiceditor.program.plugins.GraphicsConverterHolder;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class BufferedDraw extends Canvas {

    private ShapeHolder shapeHolder = ShapeHolder.getInstance();
    private DrawerHolder drawerHolder = DrawerHolder.getInstance();
    private GraphicsConverterHolder graphicsConverterHolder = GraphicsConverterHolder.getInstance();

    private DrawerState currentDrawState;


    private BufferedDraw() {
        setBackground(Color.white);
        shapeHolder.addRemoveListener(o -> update());
        shapeHolder.addCreateListener(o -> update());
        shapeHolder.addUpdateListener(o -> update());
    }

    public static BufferedDraw getInstance() {
        return BufferedDrawsSingleton.INSTANCE;
    }

    public void setState(DrawerState drawerState) {
        if (currentDrawState != null) {
            currentDrawState.changeState();
        }
        removeMouseListener(currentDrawState);
        removeMouseMotionListener(currentDrawState);
        currentDrawState = drawerState;
        addMouseListener(currentDrawState);
        addMouseMotionListener(currentDrawState);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        update();
    }


    public void update() {
        BufferStrategy bs = getBs();
        Graphics2D g = (Graphics2D) bs.getDrawGraphics();
        clearAll(g);

        g.setColor(Color.BLACK);
        g.setStroke(new BasicStroke(3.0f));
        drawAll(g);

        g.dispose();
        bs.show();
    }

    private void drawAll(Graphics g) {
        Graphics ng = g.create();
        shapeHolder.stream().forEach(shape -> draw(ng, shape));
    }

    private void draw(Graphics g, Shape shape) {
        if (shape != null) {
            Graphics ng = g.create();
            shape.settingsStream().forEach(entry ->
                    graphicsConverterHolder.getRequiredGraphicsConverter(entry.getKey()).convert(ng, entry.getValue()));
            drawerHolder.get(shape.getType()).draw(ng, shape);
        }
    }

    private BufferStrategy getBs() {
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(2);
            bs = getBufferStrategy();
        }
        return bs;
    }

    private void clearAll(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    private static class BufferedDrawsSingleton {
        private static final BufferedDraw INSTANCE = new BufferedDraw();
    }
}
