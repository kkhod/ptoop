package by.bsuir.ptoop.grphiceditor.program.plugins;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public interface DrawerState extends MouseListener, MouseMotionListener {
    String getType();

    void changeState();
}
