package by.bsuir.ptoop.grphiceditor.program.plugins;

import java.awt.*;
import java.util.List;

public interface GraphicsConverter {
    String getName();

    void convert(Graphics g, String settings);

    String getSettings();

    boolean isRequired();
}
