package by.bsuir.ptoop.grphiceditor.figure;

import by.bsuir.ptoop.grphiceditor.program.plugins.GraphicsConverter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class Shape {
    private Dot first;
    private Dot second;
    private String type;
    private Map<String, String> settings = new HashMap<>();

    public Shape() {
    }

    public Shape(Dot first, Dot second, String type) {
        this.first = first;
        this.second = second;
        this.type = type;
    }

    public Dot getFirst() {
        return first;
    }

    public Dot getSecond() {
        return second;
    }

    public String getType() {
        return type;
    }

    public void setFirst(Dot first) {
        this.first = first;
    }

    public void setSecond(Dot second) {
        this.second = second;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getSettings() {
        return settings;
    }

    public void setSettings(Map<String, String> settings) {
        this.settings = settings;
    }

    public void addSettings(GraphicsConverter graphicsConverter) {
        settings.put(graphicsConverter.getName(), graphicsConverter.getSettings());
    }

    public Stream<Map.Entry<String, String>> settingsStream() {
        return settings.entrySet().stream();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shape shape = (Shape) o;
        return Objects.equals(first, shape.first) &&
                Objects.equals(second, shape.second) &&
                Objects.equals(type, shape.type) &&
                Objects.equals(settings, shape.settings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second, type, settings);
    }
}
