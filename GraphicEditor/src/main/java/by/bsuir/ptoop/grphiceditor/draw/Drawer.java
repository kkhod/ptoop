package by.bsuir.ptoop.grphiceditor.draw;

import by.bsuir.ptoop.grphiceditor.figure.Dot;
import by.bsuir.ptoop.grphiceditor.figure.Shape;

import java.awt.*;

public interface Drawer {
    String getType();

    void draw(Graphics g, Shape shape);

    boolean isOnShape(Shape shape, Dot dot);
}
