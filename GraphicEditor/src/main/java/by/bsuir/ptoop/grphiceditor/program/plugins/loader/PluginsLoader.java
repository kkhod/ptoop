package by.bsuir.ptoop.grphiceditor.program.plugins.loader;

import by.bsuir.ptoop.grphiceditor.draw.Drawer;
import by.bsuir.ptoop.grphiceditor.draw.DrawerHolder;
import by.bsuir.ptoop.grphiceditor.figure.ShapeHolder;
import by.bsuir.ptoop.grphiceditor.program.CurrentSettings;
import by.bsuir.ptoop.grphiceditor.program.plugins.AdditionalSettingsCreator;
import by.bsuir.ptoop.grphiceditor.program.plugins.GraphicsConverterHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.ShapeModifier;
import by.bsuir.ptoop.grphiceditor.spi.LoadService;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.*;

public class PluginsLoader implements Runnable {
    private static final String DIR_PATH = "./plugins";
    private static final String JAR_REG = ".*\\.jar$";

    private DrawerHolder drawerHolder = DrawerHolder.getInstance();
    private GraphicsConverterHolder graphicsConverterHolder = GraphicsConverterHolder.getInstance();
    private CurrentSettings currentSettings = CurrentSettings.getInstance();
    private ShapeHolder shapeHolder = ShapeHolder.getInstance();

    @Override
    public void run() {
        try {
            initStaticPlugins();

            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get(DIR_PATH);
            path.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_CREATE);

            WatchKey key;
            while ((key = watchService.take()) != null) {
                for (WatchEvent<?> event : key.pollEvents()) {
                    String fileName = event.context().toString();
                    initPlugin(Paths.get(DIR_PATH, fileName));
                }
                key.reset();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void initStaticPlugins() throws IOException {
        Files.walk(Paths.get(DIR_PATH)).forEach(this::initPlugin);
    }

    private void initPlugin(Path filePath) {
        try {
            if (!Files.isDirectory(filePath) && Files.isReadable(filePath) && filePath.getFileName().toString().matches(JAR_REG)) {
                URL jarUrl = filePath.toFile().toURI().toURL();
                initDrawers(jarUrl);
                initAdditionalPanel(jarUrl);
                initShapeModifier(jarUrl);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void initDrawers(URL jarUrl) {
        LoadService<Drawer> drawerService = new LoadService<>(Drawer.class);
        drawerService.getServices(jarUrl).forEach(drawerHolder::register);
        currentSettings.updateDrawers();
    }

    private void initAdditionalPanel(URL jarUrl) {
        LoadService<AdditionalSettingsCreator> creatorsService = new LoadService<>(AdditionalSettingsCreator.class);
        creatorsService.getServices(jarUrl).forEach(additionalSettingsCreator -> {
            currentSettings.addAdditionalServices(additionalSettingsCreator.create());
            graphicsConverterHolder.add(additionalSettingsCreator.getGraphicsConverter());
        });
    }

    private void initShapeModifier(URL jarUrl) {
        LoadService<ShapeModifier> creatorsService = new LoadService<>(ShapeModifier.class);
        creatorsService.getServices(jarUrl).forEach(additionalSettingsCreator ->
                currentSettings.addAdditionalServices(additionalSettingsCreator.createModifier(shapeHolder)));
    }
}
