package by.bsuir.ptoop.grphiceditor.program;

import by.bsuir.ptoop.grphiceditor.draw.DrawerHolder;
import by.bsuir.ptoop.grphiceditor.figure.Dot;
import by.bsuir.ptoop.grphiceditor.figure.Shape;
import by.bsuir.ptoop.grphiceditor.figure.ShapeHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.DrawerState;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

public class MoveState implements DrawerState {

    private ShapeHolder shapeHolder = ShapeHolder.getInstance();
    private DrawerHolder drawerHolder = DrawerHolder.getInstance();
    private BufferedDraw bufferedDraw = BufferedDraw.getInstance();

    private by.bsuir.ptoop.grphiceditor.figure.Shape currentShape;
    private by.bsuir.ptoop.grphiceditor.figure.Shape currentHighlight;
    private boolean moving;
    private Dot prevCursorState;

    @Override
    public String getType() {
        return "Переместить";
    }

    @Override
    public void changeState() {
        reset();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            reset();
            Dot currentDot = new Dot(e.getX(), e.getY());
            currentShape = shapeHolder
                    .reverseStream()
                    .filter(shape -> drawerHolder.get(shape.getType()).isOnShape(shape, currentDot))
                    .findFirst()
                    .orElse(null);

            if (currentShape != null) {
                currentHighlight = new by.bsuir.ptoop.grphiceditor.figure.Shape(currentShape.getFirst(), currentShape.getSecond(), "Rectangle");
                Map<String, String> settings = new HashMap<>();
                settings.put("Highlight", "Highlight");
                currentHighlight.setSettings(settings);
                shapeHolder.create(currentHighlight);
            } else if (currentHighlight != null) {
                shapeHolder.remove(currentHighlight);
                currentHighlight = null;
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e) && isMouseInHighlight(e)) {
            if (currentHighlight != null) {
                moving = true;
                prevCursorState = new Dot(e.getX(), e.getY());
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (currentHighlight != null) {
                moving = false;
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        moveShape(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        changeCursor(e);
    }

    private void reset() {
        bufferedDraw.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if (currentHighlight != null) {
            shapeHolder.remove(currentHighlight);
        }
        currentHighlight = null;
        currentShape = null;
    }

    private void changeCursor(MouseEvent e) {
        if (currentHighlight != null) {
            if (isMouseInHighlight(e)) {
                bufferedDraw.setCursor(new Cursor(Cursor.MOVE_CURSOR));
            } else {
                bufferedDraw.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }

    private void moveShape(MouseEvent e) {
        if (currentHighlight != null && moving) {
            moveShape(currentShape, e);
            moveShape(currentHighlight, e);
            prevCursorState = new Dot(e.getX(), e.getY());
        }
    }

    private void moveShape(Shape shape, MouseEvent e) {
        Dot first = shape.getFirst();
        Dot second = shape.getSecond();
        Dot currentCursorState = new Dot(e.getX(), e.getY());
        int deltaX = prevCursorState.getX() - currentCursorState.getX();
        int deltaY = prevCursorState.getY() - currentCursorState.getY();
        shape.setFirst(new Dot(first.getX() - deltaX, first.getY() - deltaY));
        shape.setSecond(new Dot(second.getX() - deltaX, second.getY() - deltaY));
        shapeHolder.update(shape);
    }

    private boolean isMouseInHighlight(MouseEvent e) {
        if (currentShape != null) {
            int x1 = Math.min(currentHighlight.getFirst().getX(), currentHighlight.getSecond().getX());
            int y1 = Math.min(currentHighlight.getFirst().getY(), currentHighlight.getSecond().getY());
            int x2 = Math.max(currentHighlight.getFirst().getX(), currentHighlight.getSecond().getX());
            int y2 = Math.max(currentHighlight.getFirst().getY(), currentHighlight.getSecond().getY());
            int x = e.getX();
            int y = e.getY();

            return x >= x1 && x <= x2 && y >= y1 && y <= y2;
        } else {
            return false;
        }
    }
}
