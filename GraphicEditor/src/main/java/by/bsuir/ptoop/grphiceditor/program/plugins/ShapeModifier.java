package by.bsuir.ptoop.grphiceditor.program.plugins;

import by.bsuir.ptoop.grphiceditor.figure.ShapeHolder;

import javax.swing.*;

public interface ShapeModifier {
    JPanel createModifier(ShapeHolder shapeHolder);
}
