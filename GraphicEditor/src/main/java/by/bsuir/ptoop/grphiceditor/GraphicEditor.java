package by.bsuir.ptoop.grphiceditor;

import by.bsuir.ptoop.grphiceditor.program.HighlightConverter;
import by.bsuir.ptoop.grphiceditor.program.WindowHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.GraphicsConverterHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.loader.PluginsLoader;

import javax.swing.*;

public class GraphicEditor {
    public static void main(String[] args) {
        GraphicsConverterHolder.getInstance().add(new HighlightConverter());
        SwingUtilities.invokeLater(() -> {
            WindowHolder initializer = new WindowHolder();
            initializer.init();
            Thread pluginWatcher = new Thread(new PluginsLoader());
            pluginWatcher.start();
        });

    }
}
