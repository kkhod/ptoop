package by.bsuir.ptoop.grphiceditor.program.plugins;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public final class GraphicsConverterHolder {
    private Map<String, GraphicsConverter> requiredGraphicsConverters = new HashMap<>();
    private Map<String, GraphicsConverter> optionalGraphicsConverters = new HashMap<>();

    private GraphicsConverterHolder() {
    }

    public static GraphicsConverterHolder getInstance() {
        return GraphicsConverterHolderSingleton.INSTANCE;
    }

    public void add(GraphicsConverter graphicsConverter) {
        if (graphicsConverter.isRequired()) {
            requiredGraphicsConverters.put(graphicsConverter.getName(), graphicsConverter);
        } else {
            optionalGraphicsConverters.put(graphicsConverter.getName(), graphicsConverter);
        }
    }

    public Stream<GraphicsConverter> stream() {
        return requiredGraphicsConverters.values().stream();
    }

    public GraphicsConverter getRequiredGraphicsConverter(String name) {
        GraphicsConverter graphicsConverter = requiredGraphicsConverters.get(name);
        if (graphicsConverter == null) {
            graphicsConverter = optionalGraphicsConverters.get(name);
        }
        return graphicsConverter;
    }

    private static class GraphicsConverterHolderSingleton {
        private static final GraphicsConverterHolder INSTANCE = new GraphicsConverterHolder();
    }
}
