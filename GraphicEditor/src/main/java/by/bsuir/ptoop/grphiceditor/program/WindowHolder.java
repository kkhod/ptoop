package by.bsuir.ptoop.grphiceditor.program;

import by.bsuir.ptoop.grphiceditor.draw.Drawer;
import by.bsuir.ptoop.grphiceditor.draw.DrawerHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.DrawerState;
import by.bsuir.ptoop.grphiceditor.spi.LoadService;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.Collections;

public class WindowHolder {
    private DrawerHolder drawerHolder = DrawerHolder.getInstance();
    private CurrentSettings currentSettings = CurrentSettings.getInstance();

    private JFrame mainFrame;
    private JPanel settingsDraw;
    private JPanel drawers;
    private JPanel mainPanel;

    public void init() {
        currentSettings.setInitializer(this);

        initDrawerServices();
        initFrame();
        initDrawSettings();
        initDrawersPanel();
        initDrawers();
        initMainPanel();
        initPaintPanel();
        pack();
    }

    private void initDrawerServices() {
        LoadService<Drawer> graphicsConverterService = new LoadService<>(Drawer.class);
        graphicsConverterService.getServices().forEach(drawerHolder::register);
    }

    private void initFrame() {
        mainFrame = new JFrame("Графический редактор");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        mainFrame.setPreferredSize(new Dimension(800, 600));
        mainFrame.setResizable(true);
        mainFrame.setLayout(new GridBagLayout());
    }

    private void initDrawSettings() {
        GridBagConstraints c = new GridBagConstraints();
        settingsDraw = new JPanel(new FlowLayout(FlowLayout.LEFT));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.3;
        mainFrame.add(settingsDraw, c);
    }

    private void initDrawersPanel() {
        drawers = new JPanel();
        drawers.setSize(new Dimension(250, 85));
        drawers.setLocation(0, 8);
        settingsDraw.add(drawers);
    }

    private void initDrawers() {
        ButtonGroup group = new ButtonGroup();
        JPanel p = new JPanel(new GridLayout(3, 10));
        p.setSize(drawers.getWidth(), drawers.getHeight());
        DrawerHolder.getInstance().stream().forEach(drawer -> {
            JToggleButton tb = new JToggleButton(drawer.getType());
            tb.addActionListener(e -> CurrentSettings.getInstance().setCurrentDrawer(drawer));
            group.add(tb);
            p.add(tb);
        });

        Collections.list(group.getElements()).stream().findFirst().ifPresent(abstractButton -> {
            abstractButton.setSelected(true);
            abstractButton.doClick();
        });

        drawers.add(p);
    }

    void updateDrawers() {
        drawers.removeAll();
        initDrawers();
        drawers.updateUI();
    }

    private void initMainPanel() {
        GridBagConstraints c = new GridBagConstraints();
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setBorder(new LineBorder(new Color(238, 238, 238), 5));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.5;
        c.weighty = 0.5;
        c.ipady = 1000;
        mainFrame.add(mainPanel, c);
    }

    private void initPaintPanel() {
        GridBagConstraints c = new GridBagConstraints();
        BufferedDraw canvas = BufferedDraw.getInstance();
        ButtonGroup group = new ButtonGroup();
        JPanel p = new JPanel(new GridLayout(3, 10));
        p.setSize(100, 100);
        DrawerState drawerState = new DrawState();
        JToggleButton draw = new JToggleButton(drawerState.getType());
        draw.addActionListener(e -> BufferedDraw.getInstance().setState(drawerState));
        group.add(draw);
        p.add(draw);
        DrawerState moveState = new MoveState();
        JToggleButton move = new JToggleButton(moveState.getType());
        move.addActionListener(e -> BufferedDraw.getInstance().setState(moveState));
        group.add(move);
        p.add(move);
        Collections.list(group.getElements()).stream().findFirst().ifPresent(abstractButton -> {
            abstractButton.setSelected(true);
            abstractButton.doClick();
        });
        settingsDraw.add(p);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.5;
        c.weighty = 0.5;
        c.ipady = 1000;

        mainPanel.add(canvas, c);
    }

    void addAdditionalServices(JPanel additionalSettingsPanel) {
        settingsDraw.add(additionalSettingsPanel);
        settingsDraw.updateUI();
    }

    private void pack() {
        mainFrame.pack();
        mainFrame.setLocationByPlatform(true);
        mainFrame.setVisible(true);
    }
}
