package by.bsuir.ptoop.graphiceditor.arrowdrawerplugin;

import by.bsuir.ptoop.grphiceditor.draw.Drawer;
import by.bsuir.ptoop.grphiceditor.figure.Dot;
import by.bsuir.ptoop.grphiceditor.figure.Shape;

import java.awt.*;

public class ArrowDrawer implements Drawer {
    @Override
    public String getType() {
        return "Стрелка";
    }

    @Override
    public void draw(Graphics g, Shape shape) {
        Dot first = shape.getFirst();
        Dot second = shape.getSecond();
        int x1 = first.getX();
        int x2 = second.getX();
        int y1 = first.getY();
        int y2 = second.getY();

        Dot d1 = new Dot(x1, y1 + (y2 - y1) / 4);
        Dot d2 = new Dot(x1 + (x2 - x1) / 2, y1 + (y2 - y1) / 4);
        Dot d3 = new Dot(x1 + (x2 - x1) / 2, y1);
        Dot d4 = new Dot(x2, y1 + (y2 - y1) / 2);
        Dot d5 = new Dot(x1 + (x2 - x1) / 2, y2);
        Dot d6 = new Dot(x1 + (x2 - x1) / 2, y2 - (y2 - y1) / 4);
        Dot d7 = new Dot(x1, y2 - (y2 - y1) / 4);

        g.drawLine(d1.getX(), d1.getY(), d2.getX(), d2.getY());
        g.drawLine(d2.getX(), d2.getY(), d3.getX(), d3.getY());
        g.drawLine(d3.getX(), d3.getY(), d4.getX(), d4.getY());
        g.drawLine(d4.getX(), d4.getY(), d5.getX(), d5.getY());
        g.drawLine(d5.getX(), d5.getY(), d6.getX(), d6.getY());
        g.drawLine(d6.getX(), d6.getY(), d7.getX(), d7.getY());
        g.drawLine(d7.getX(), d7.getY(), d1.getX(), d1.getY());
    }

    @Override
    public boolean isOnShape(Shape shape, Dot dot) {
        return false;
    }
}
