package by.bsuir.ptoop.graphiceditor.basicdrawer;

import by.bsuir.ptoop.grphiceditor.draw.Drawer;
import by.bsuir.ptoop.grphiceditor.figure.Dot;
import by.bsuir.ptoop.grphiceditor.figure.Shape;

import java.awt.*;

public class RhombusDrawer implements Drawer {
    @Override
    public String getType() {
        return "Ромб";
    }

    @Override
    public void draw(Graphics g, Shape shape) {
        Dot first = new Dot(shape.getFirst().getX() + (shape.getSecond().getX() - shape.getFirst().getX()) / 2,
                shape.getFirst().getY());
        Dot second = new Dot(shape.getSecond().getX(),
                shape.getFirst().getY() + (shape.getSecond().getY() - shape.getFirst().getY()) / 2);
        Dot third = new Dot(shape.getFirst().getX() + (shape.getSecond().getX() - shape.getFirst().getX()) / 2,
                shape.getSecond().getY());
        Dot fourth = new Dot(shape.getFirst().getX(),
                shape.getFirst().getY() + (shape.getSecond().getY() - shape.getFirst().getY()) / 2);

        g.drawLine(first.getX(), first.getY(), second.getX(), second.getY());
        g.drawLine(second.getX(), second.getY(), third.getX(), third.getY());
        g.drawLine(third.getX(), third.getY(), fourth.getX(), fourth.getY());
        g.drawLine(fourth.getX(), fourth.getY(), first.getX(), first.getY());
    }

    @Override
    public boolean isOnShape(Shape shape, Dot dot) {
        return false;
    }
}
