package by.bsuir.ptoop.graphiceditor.basicdrawer;

import by.bsuir.ptoop.grphiceditor.draw.Drawer;
import by.bsuir.ptoop.grphiceditor.figure.Dot;
import by.bsuir.ptoop.grphiceditor.figure.Shape;

import java.awt.*;

public class TriangleDrawer implements Drawer {
    @Override
    public String getType() {
        return "Треугольник";
    }

    @Override
    public void draw(Graphics g, Shape shape) {
        Dot first = new Dot(shape.getFirst().getX(), shape.getSecond().getY());
        Dot second = shape.getSecond();
        Dot third = new Dot(shape.getFirst().getX() + (second.getX() - shape.getFirst().getX()) / 2,
                shape.getFirst().getY());

        g.drawLine(first.getX(), first.getY(), second.getX(), second.getY());
        g.drawLine(second.getX(), second.getY(), third.getX(), third.getY());
        g.drawLine(third.getX(), third.getY(), first.getX(), first.getY());
    }

    @Override
    public boolean isOnShape(Shape shape, Dot dot) {
        return false;
    }
}
