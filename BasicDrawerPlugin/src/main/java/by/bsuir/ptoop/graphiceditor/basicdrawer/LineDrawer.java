package by.bsuir.ptoop.graphiceditor.basicdrawer;

import by.bsuir.ptoop.grphiceditor.draw.Drawer;
import by.bsuir.ptoop.grphiceditor.figure.Dot;
import by.bsuir.ptoop.grphiceditor.figure.Shape;

import java.awt.*;

public class LineDrawer implements Drawer {
    @Override
    public String getType() {
        return "Линия";
    }

    @Override
    public void draw(Graphics g, Shape shape) {
        Dot first = shape.getFirst();
        Dot second = shape.getSecond();
        g.drawLine(first.getX(), first.getY(), second.getX(), second.getY());
    }

    @Override
    public boolean isOnShape(Shape shape, Dot dot) {
        double x = dot.getX();
        double y = dot.getY();
        double x1 = shape.getFirst().getX();
        double y1 = shape.getFirst().getY();
        double x2 = shape.getSecond().getX();
        double y2 = shape.getSecond().getY();

        double dx1 = x2 - x1;
        double dy1 = y2 - y1;

        double dx = x - x1;
        double dy = y - y1;

        double S = dx1 * dy - dx * dy1;
        return Math.abs(S) < 1000;
    }
}
