package by.bsuir.ptoop.graphiceditor.basicdrawer;

import by.bsuir.ptoop.grphiceditor.draw.Drawer;
import by.bsuir.ptoop.grphiceditor.figure.Dot;
import by.bsuir.ptoop.grphiceditor.figure.Shape;

import java.awt.*;

public class CircleDrawer implements Drawer {
    @Override
    public String getType() {
        return "Круг";
    }

    @Override
    public void draw(Graphics g, Shape shape) {
        Dot first = shape.getFirst();
        Dot second = shape.getSecond();
        int width = Math.abs(second.getX() - first.getX());
        int height = Math.abs(second.getY() - first.getY());
        g.drawOval(Math.min(first.getX(), second.getX()), Math.min(first.getY(), second.getY()), width, height);
    }

    @Override
    public boolean isOnShape(Shape shape, Dot dot) {
        return false;
    }
}
