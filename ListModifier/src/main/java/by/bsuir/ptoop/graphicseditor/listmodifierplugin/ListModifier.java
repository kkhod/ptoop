package by.bsuir.ptoop.graphicseditor.listmodifierplugin;

import by.bsuir.ptoop.grphiceditor.figure.ShapeHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.ShapeModifier;

import javax.swing.*;
import java.awt.*;

public class ListModifier implements ShapeModifier {
    @Override
    public JPanel createModifier(ShapeHolder shapeHolder) {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        shapeHolder.stream().forEach(shape -> listModel.addElement(shape.getType()));
        JList<String> list = new JList<>(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(-1);

        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(100, 100));

        JButton deleteButton = new JButton("Удалить");
        deleteButton.addActionListener(e -> {
            int selectedIndex = list.getSelectedIndex();
            if (selectedIndex != -1) {
                shapeHolder.remove(selectedIndex);
                if (selectedIndex > 0) {
                    list.setSelectedIndex(--selectedIndex);
                } else if (selectedIndex == 0 && !listModel.isEmpty()) {
                    list.setSelectedIndex(selectedIndex);
                }
            }
        });

        JPanel panel = new JPanel();
        panel.add(listScroller);
        panel.add(deleteButton);

        shapeHolder.addCreateListener(shape -> listModel.addElement(shape.getType()));
        shapeHolder.addRemoveListener(listModel::removeElementAt);

        return panel;
    }
}
