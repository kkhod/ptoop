package by.bsuir.ptoop.graphiceditor.listsaver;

import by.bsuir.ptoop.grphiceditor.figure.Shape;
import by.bsuir.ptoop.grphiceditor.figure.ShapeHolder;
import by.bsuir.ptoop.grphiceditor.program.plugins.ShapeModifier;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class JsonListFileSystem implements ShapeModifier {
    private static final String JSON = "json";

    @Override
    public JPanel createModifier(ShapeHolder shapeHolder) {
        JFileChooser fc = new JFileChooser();
        fc.addChoosableFileFilter(new FileNameExtensionFilter(JSON, JSON));
        fc.setAcceptAllFileFilterUsed(false);

        JButton saveButton = new JButton("Сохранить");
        JButton openButton = new JButton("Открыть");

        JPanel panel = new JPanel();
        saveButton.addActionListener(e -> {
            int selectedOption = fc.showSaveDialog(panel);
            if (selectedOption == JFileChooser.APPROVE_OPTION) {
                save(fc, shapeHolder);
            }
        });

        openButton.addActionListener(e -> {
            int selectedOption = fc.showOpenDialog(panel);
            if (selectedOption == JFileChooser.APPROVE_OPTION) {
                shapeHolder.removeAll();
                open(fc, shapeHolder);
            }
        });
        panel.add(saveButton);
        panel.add(openButton);

        return panel;
    }

    private void save(JFileChooser fc, ShapeHolder shapeHolder) {
        String format = fc.getFileFilter().getDescription();
        Path path = Paths.get(fc.getCurrentDirectory().toString());
        path = path.resolve(fc.getSelectedFile().getName() + "." + format);

        try {
            if (format.equals(JSON)) {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.writeValue(path.toFile(), shapeHolder.stream().collect(Collectors.toList()));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void open(JFileChooser fc, ShapeHolder shapeHolder) {
        String format = fc.getFileFilter().getDescription();
        Path path = Paths.get(fc.getCurrentDirectory().toString());
        path = path.resolve(fc.getSelectedFile().getName());

        try {
            if (format.equals(JSON)) {
                ObjectMapper objectMapper = new ObjectMapper();
                shapeHolder.createAll(objectMapper.readValue(path.toFile(), new TypeReference<List<Shape>>() {
                }));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}