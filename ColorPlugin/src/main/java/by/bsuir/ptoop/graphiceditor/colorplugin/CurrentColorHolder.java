package by.bsuir.ptoop.graphiceditor.colorplugin;

import java.awt.*;

final class CurrentColorHolder {
    private Color currentColor = Color.BLACK;

    private CurrentColorHolder() {
    }

    static CurrentColorHolder getInstance() {
        return CurrentColorHolderSingleton.INSTANCE;
    }

    Color getCurrentColor() {
        return currentColor;
    }

    void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }

    private static class CurrentColorHolderSingleton {
        private static final CurrentColorHolder INSTANCE = new CurrentColorHolder();
    }
}
