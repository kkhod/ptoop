package by.bsuir.ptoop.graphiceditor.colorplugin;

import by.bsuir.ptoop.grphiceditor.program.plugins.AdditionalSettingsCreator;
import by.bsuir.ptoop.grphiceditor.program.plugins.GraphicsConverter;

import javax.swing.*;
import java.awt.*;

public class ColorPanel implements AdditionalSettingsCreator {
    private Color currentColor = Color.BLACK;

    Color getCurrentColor() {
        return currentColor;
    }

    private void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }

    @Override
    public JPanel create() {
        JPanel panel = new JPanel();
        ColorChooserButton colorChooser = new ColorChooserButton(currentColor);
        colorChooser.addColorChangedListener(this::setCurrentColor);
        panel.add(colorChooser);
        return panel;
    }

    @Override
    public GraphicsConverter getGraphicsConverter() {
        return new ColorGraphicConverter(this);
    }
}
