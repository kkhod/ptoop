package by.bsuir.ptoop.graphiceditor.colorplugin;

public class ShapeColor {
    private int RGB;

    public int getRGB() {
        return RGB;
    }

    public void setRGB(int RGB) {
        this.RGB = RGB;
    }
}
