package by.bsuir.ptoop.graphiceditor.colorplugin;

import by.bsuir.ptoop.grphiceditor.program.plugins.GraphicsConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.awt.*;
import java.io.IOException;

public class ColorGraphicConverter implements GraphicsConverter {
    private ColorPanel colorPanel;

    ColorGraphicConverter(ColorPanel colorPanel) {
        this.colorPanel = colorPanel;
    }

    @Override
    public String getName() {
        return "Цвет";
    }

    @Override
    public void convert(Graphics g, String settings) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ShapeColor shapeColor = mapper.readValue(settings, ShapeColor.class);
            Color color = convert(shapeColor);
            g.setColor(color);
        } catch (IOException ignored) {
        }
    }

    @Override
    public String getSettings() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(convert(colorPanel.getCurrentColor()));
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    @Override
    public boolean isRequired() {
        return true;
    }

    private ShapeColor convert(Color color) {
        ShapeColor shapeColor = new ShapeColor();
        shapeColor.setRGB(color.getRGB());
        return shapeColor;
    }

    private Color convert(ShapeColor shapeColor) {
        return new Color(shapeColor.getRGB());
    }
}
