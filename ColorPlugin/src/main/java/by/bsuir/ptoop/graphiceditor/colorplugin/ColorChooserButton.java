package by.bsuir.ptoop.graphiceditor.colorplugin;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

class ColorChooserButton extends JButton {

    private Color current;

    ColorChooserButton(Color c) {
        setSelectedColor(c);
        addActionListener(arg0 -> {
            Color newColor = JColorChooser.showDialog(null, "Выберите цвет", current);
            setSelectedColor(newColor);
        });
    }

    private void setSelectedColor(Color newColor) {
        if (newColor == null) return;

        current = newColor;
        setIcon(createIcon(current));
        repaint();

        for (ColorChangedListener l : listeners) {
            l.colorChanged(newColor);
        }
    }

    public interface ColorChangedListener {
        void colorChanged(Color newColor);
    }

    private List<ColorChangedListener> listeners = new ArrayList<>();

    void addColorChangedListener(ColorChangedListener toAdd) {
        listeners.add(toAdd);
    }

    private static ImageIcon createIcon(Color main) {
        BufferedImage image = new BufferedImage(16, 16, java.awt.image.BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = image.createGraphics();
        graphics.setColor(main);
        graphics.fillRect(0, 0, 16, 16);
        graphics.setXORMode(Color.DARK_GRAY);
        graphics.drawRect(0, 0, 16 - 1, 16 - 1);
        image.flush();
        return new ImageIcon(image);
    }
}